angular.module('wishpool').controller('sharedController', ['$scope', '$window', 'sharedService',
    function ($scope, $window, sharedService) {
        $scope.showNav = function(){
            var logged = sharedService.checkLogin();
            if (logged){
                return true
            }
            else{
                return false
            }
        };

        $scope.signOut = function () {
            localStorage.removeItem('wishpoolUser');
            $window.location.href = '#/'
        }
    }]);